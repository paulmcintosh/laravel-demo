<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    return [
		'name' => $faker->word,
		'description' => $faker->sentence,
		'price' => $faker->randomFloat( 2, 0, 100 ),
		'status' => $faker->randomElement( [ 0, 1 ] ),
		'image_url' => $faker->url,
		'slug' => $faker->slug,
    ];
});
