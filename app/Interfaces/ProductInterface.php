<?php
/**
 * Created by: Ben Lewis
 * Date: 22/05/2019
 * Time: 09:46
 */

namespace App\Interfaces;

use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;

interface ProductInterface
{
	/**
	 * Return a collection of products
	 * @param int $limit. Limit total products returned
	 * @param int $offset. Offset starting point for products returned
	 * @param array $statuses. Product statuses to constrain return
	 * @return Product[]
	 */
	public function all( int $limit, int $offset, array $statuses );

	/**
	 * Find a specific Order
	 * @param int $id
	 * @return Product
	 * @throws ModelNotFoundException
	 */
	public function findById( int $id );
}